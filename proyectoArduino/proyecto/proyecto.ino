#include <Adafruit_Sensor.h>

#include <DHT.h>
// pin del sensor de humedad y temperatura en el aire
#define DHTPIN 2
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);
const int sensorTierraHumedad = A0;
const int sensorNivelAgua = A2;
const int bombaAgua = 8;
int serialRead;
int aguaValor;
int humedadTierra;
int luzPin = A1;
int luzValor ;
void setup() {
  // put your setup code here, to run once:
  pinMode(bombaAgua,OUTPUT);
  Serial.begin(57600);
  dht.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available())
  {
    serialRead = Serial.read();
    if(serialRead='f')
    {
      analogWrite(bombaAgua,255);
      delay(8000);
      analogWrite(bombaAgua,0);
    }
  }
  else delay(1000);
  humedadTierra = analogRead(sensorTierraHumedad);
  humedadTierra = map(humedadTierra,0,1023,100,0);
  luzValor = analogRead(luzPin);
  luzValor = map(luzValor,0,1023,0,255);
  aguaValor = analogRead(sensorNivelAgua);
  aguaValor = map(aguaValor,0,1021,0,100);
  // Leemos la humedad relativa
  float h = dht.readHumidity();
  // Leemos la temperatura en grados centígrados (por defecto)
  float t = dht.readTemperature();
  // Leemos la temperatura en grados Fahrenheit
  float f = dht.readTemperature(true);


  // Calcular el índice de calor en Fahrenheit
  float hif = dht.computeHeatIndex(f, h);
  // Calcular el índice de calor en grados centígrados
  float hic = dht.computeHeatIndex(t, h, false);
  Serial.print("A");//humedadTierra
  Serial.println(humedadTierra);
  //temperaturaAire
  Serial.print("B");
  //temperaturaAire
  Serial.println(t);
  //humedadAire
  Serial.print("C");
  Serial.println(h);
  //Luz
  Serial.print("D");
  Serial.println(luzValor);
  //Agua
  Serial.print("E");
  Serial.println(aguaValor);
  
}
