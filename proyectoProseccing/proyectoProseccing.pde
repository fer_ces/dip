// Need G4P library
import g4p_controls.*;
import processing.serial.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Archivo_historial{
  int hh,mm,ss,dd;
  float temperatura, humedad_a, humedad_t, luz;
  boolean riego;
  
  public Archivo_historial(float temp, float ha, float ht, float luz, boolean r){
    this.hh = hour();
    this.mm = minute();
    this.ss = second();
    this.dd = day();
    
    this.temperatura = temp;
    this.humedad_a = ha;
    this.humedad_t = ht;
    this.luz = luz;
    this.riego = r;
  }
  
  public float temp(){return temperatura;}
  public float ha(){return humedad_a;}
  public float ht(){return humedad_t;}
  public float luz(){return luz;}
  public String hora(){return "Dia "+dd+", "+hh+":"+mm+":"+ss;}
}

String buff = "";
Serial port;
int saltoLinea = 10;

//Valores de estado actual
int valTemperatura = 0;
int valHumedadAire = 0;
int valHumedadTierra = 0;
int valLuz = 0;
int valAgua = 0;
float deposito = 0.2;
  
String temperatura = "";
String humedadAire = "";
String humedadTierra = "";
String luzS = "";
String aguaS = "";

//Arrays varios
int[] valuesHumedadAire = new int[64];
int[] values = new int[64];
int[] valuesHumedadTierra = new int[64];
String[] riegos = new String[5];

//Strings para el timepo
String tiempoActual;

//valores para configurar los riegos automaticos
Boolean autoRiegoActivado = false;
int valorHumedadTierraRiego = 0;
long tiempoUltimoRiego = 0;
long actualizacionValores = 0;
//variable para controlar el riego manual
Boolean regarManual = false;

String showData = "";

//Historial
Archivo_historial[] historial = new Archivo_historial[24];
int ultimaHora = 0;
int ultimaHoraRiego = 0;


public void setup() {
  size(600, 700, JAVA2D);
  createGUI();
  customGUI();
  // Place your setup code here
   port = new Serial(this, "COM7",57600);
   try{
   Thread.sleep(3000); 
   }catch(InterruptedException e)
   {
<<<<<<< HEAD
   }*/
   
   //Inicializa el historial a 0
   for(int i = 0; i < 23; i++){
     historial[i] = new Archivo_historial(0.0,0.0,0.0,0.0,false);
=======
>>>>>>> 8b6a68ad95708575a0a69ca062cfde23d52fcf01
   }
}

public void draw() {
  background(230);
  
  
  switch(showData) {
  case "":
    break;
  case "stats":
    showStats();
    fill(255, 255, 255);
    break;
  case "historico":
    showHistorico();
    fill(255, 255, 255);
    break;
  }
  
  //Actualiza el historial cada hora
  if(ultimaHora != hour()) guardaHistorial();

  while(port.available() > 0)
   serialEvent(port.read());
   println(humedadTierra);
   if(autoRiegoActivado && System.currentTimeMillis() - tiempoUltimoRiego > 3600000 && valorHumedadTierraRiego > int(humedadTierra) && System.currentTimeMillis() - actualizacionValores > 1000  || regarManual)
  {
    println("Riego");
    port.write('f');
    tiempoUltimoRiego = System.currentTimeMillis();
      actualizarRiegos();
      
  ultimoRiego.setText("Ultimo riego realizado el " + tiempoActual);
   regarManual = false;
  }
    
}

// Use this method to add additional statements
// to customise the GUI controls
public void customGUI() {
}



public void serialEvent(int serial)
{
  if (serial != saltoLinea) {
    buff += char(serial);
  } else {
    char c = buff.charAt(0);
    buff = buff.substring(1);
    buff = buff.substring(0, buff.length() -1);
    if (c == 'C')humedadAire = buff;
    if (c == 'B')temperatura = buff;
    if (c == 'A') humedadTierra = buff;
    if (c == 'D') luzS = buff;
    if (c == 'E') aguaS = buff;
    try { 
      valTemperatura = int(temperatura);
      valHumedadAire =  int(humedadAire);
      valHumedadTierra = int(humedadTierra);
      valLuz = int(luzS);
      valAgua = int(aguaS);
    }
    catch(NumberFormatException e) {
    }
    buff="";
    for (int i = 0; i < 63; i++) {
      values[i] = values[i+1];
      valuesHumedadAire[i] = valuesHumedadAire[i + 1];
      valuesHumedadTierra[i] = valuesHumedadTierra[i+1];
    }
    values[63] = valTemperatura; 
    valuesHumedadAire[63] = valHumedadAire;
    valuesHumedadTierra[63] = valHumedadTierra;
  }
}



public void showStats()
{
  //Grafico temperatura
  fill(0, 0, 0);
  textAlign(LEFT);
  text("Temperatura: " + valTemperatura + " ºC", 0.27*width, 0.02*height);
  fill(255, 255, 255);
  rect(0.27*width, 0.03*height, 0.4*width, 0.15*height);
  dibujarLinea(int(0.27*width), int(0.03*height), int(0.4*width), int(0.15*height), values);

  //Grafico Humedad
  fill(0, 0, 0);
  text("Humedad en Aire: "+ valHumedadAire + " %", 0.27*width, 0.22*height );
  fill(255, 255, 255);
  rect(0.27*width, 0.23*height, 0.4*width, 0.15*height);
  dibujarLinea(int(0.27*width), int(0.23*height), int(0.4*width), int(0.15*height), valuesHumedadAire);

  //Grafico Agua
  fill(0, 0, 0);
  text("Agua: ", 0.27*width, 0.42*height );
  fill(255, 255, 255);
  rect(0.27*width, 0.43*height, 0.08*width, 0.25*height);
  fill(52, 182, 216);
<<<<<<< HEAD
  rect(0.27*width, 0.43*height+0.25*height, 0.08*width, -1*0.25*height*(valAgua*deposito/100));
=======
  rect(0.27*width, 0.43*height+0.25*height, 0.08*width, -1*0.25*height*(agua/100*2 ));
>>>>>>> 8b6a68ad95708575a0a69ca062cfde23d52fcf01

  //Grafico Luz
  fill(0, 0, 0);
  int auxLuz = valLuz * 100/255;
  text("Luz: " + auxLuz + " %", 0.45*width, 0.42*height );
  fill(255, 255, 255);
  ellipseMode(CORNER);
  fill(255, 255, 255-valLuz);
  ellipse(0.45*width, 0.43*height, 0.1*width, 0.1*height);

  //Grafico Humedad en Tierra
  fill(0, 0, 0);
  text("Humedad en Tierra: "+ valHumedadTierra + " %", 0.27*width, 0.72*height );
  fill(255, 255, 255);
  rect(0.27*width, 0.73*height, 0.4*width, 0.15*height);  
  dibujarLinea(int(0.27*width), int(0.73*height), int(0.4*width), int(0.15*height), valuesHumedadTierra);
}

public void showHistorico()
{
  int horaActual = hour();
  
  //Grafico temperatura
  fill(0, 0, 0);
  textAlign(LEFT);
  textSize(11);
  text("Temperatura ultimas 24h (ºC):", 0.27*width, 0.02*height);
  fill(255, 255, 255);
  rect(0.27*width, 0.03*height, 0.4*width, 0.15*height);
  dibujarHistorial(int(0.27*width), int(0.03*height), int(0.4*width), int(0.15*height), "temp");
  fill(255,0,0);
  noStroke();
  //rect(0.27*width+horaActual*(0.4*width/24), 0.03*height, 1, 0.15*height);
  textSize(8);
  text(horaActual + "h",0.27*width+horaActual*(0.4*width/24)+2, 0.05*height);
  stroke(0);

  //Grafico Humedad Aire
  fill(0, 0, 0);
  textSize(11);
  text("Humedad en Aire ultimas 24h (%):", 0.27*width, 0.22*height );
  fill(255, 255, 255);
  rect(0.27*width, 0.23*height, 0.4*width, 0.15*height);
  dibujarHistorial(int(0.27*width), int(0.23*height), int(0.4*width), int(0.15*height), "ha");
  fill(255,0,0);
  noStroke();
  rect(0.27*width+horaActual*(0.4*width/24), 0.23*height, 1, 0.15*height);
  textSize(8);
  text(horaActual + "h",0.27*width+horaActual*(0.4*width/24)+2, 0.25*height);
  stroke(0);

  //Grafico Humedad Tierra
  fill(0, 0, 0);
  textSize(11);
  text("Humedad en Tierra ultimas 24h (%): ", 0.27*width, 0.42*height );
  fill(255, 255, 255);
  rect(0.27*width, 0.43*height, 0.4*width, 0.15*height);
  dibujarHistorial(int(0.27*width), int(0.43*height), int(0.4*width), int(0.15*height), "ht");
  fill(255,0,0);
  noStroke();
  textSize(8);
  rect(0.27*width+horaActual*(0.4*width/24), 0.43*height, 1, 0.15*height);
  textSize(8);
  text(horaActual + "h",0.27*width+horaActual*(0.4*width/24)+2, 0.45*height);
  stroke(0);

  //Grafico Luz
  fill(0, 0, 0);
  textSize(11);
  text("Luz ultimas 24h: (%)", 0.27*width, 0.62*height );
  fill(255, 255, 255);
  rect(0.27*width, 0.63*height, 0.4*width, 0.15*height);
  dibujarHistorial(int(0.27*width), int(0.63*height), int(0.4*width), int(0.15*height), "luz");
  fill(255,0,0);
  noStroke();
  rect(0.27*width+horaActual*(0.4*width/24), 0.63*height, 1, 0.15*height);
  textSize(8);
  text(horaActual + "h",0.27*width+horaActual*(0.4*width/24)+2, 0.65*height);
  stroke(0);
  textSize(11);

  //Horas Riego
  fill(0, 0, 0);
  text("Riegos realizados:", 0.27*width, 0.82*height);
  for (int i = 0; i < 5; i++) {
    try {
      text(1 + i + "  " + riegos[i], 0.27*width, 0.84*height + 0.02*height*i);
    }
    catch(Exception e) {
    }
  }
}

public void dibujarLinea(int posX, int posY, int sizeX, int sizeY, int value[])
{
  int aux = sizeX / value.length;
  int aux2 = sizeY / 100;
  for (int i = 0; i < value.length - 1; i++) 
  {
    line(i*aux+posX, posY+sizeY - value[i]*aux2, (i + 1)*aux + posX, posY-value[i +1]*aux2 + sizeY);
  }
}

public void dibujarHistorial(int posX, int posY, int sizeX, int sizeY, String variable)
{
  int aux = sizeX / 24;
  int aux2 = sizeY / 50;
  switch(variable){
    case "temp":for (int i = 0; i < 22; i++)
                  line(i*aux+posX, posY+sizeY - historial[i].temp()*aux2, (i + 1)*aux + posX, posY-historial[i+1].temp()*aux2 + sizeY);
                break;
    case "ha":for (int i = 0; i < 22; i++) 
                  line(i*aux+posX, posY+sizeY - historial[i].ha()*aux2, (i + 1)*aux + posX, posY-historial[i+1].ha()*aux2 + sizeY);
                break;    
    case "ht":for (int i = 0; i < 22; i++) 
                  line(i*aux+posX, posY+sizeY - historial[i].ht()*aux2, (i + 1)*aux + posX, posY-historial[i+1].ht()*aux2 + sizeY);
                break;
    case "luz":for (int i = 0; i < 22; i++) 
                  line(i*aux+posX, posY+sizeY - historial[i].luz()*aux2, (i + 1)*aux + posX, posY-historial[i+1].luz()*aux2 + sizeY);
                break;
  }
}

//metodo para obtener el tiempo y la fecha actual
public String getTime() {
  Calendar cal = Calendar.getInstance();
  SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'a las' HH:mm:ss");

  String tiempoAux = sdf.format(cal.getTime());
  ;
  return tiempoAux;
}


//metodo para actualizar los riegos

public void actualizarRiegos()
{
  tiempoActual = getTime();
  for (int i = 0; i < riegos.length - 1; i++)
  {
    riegos[i] = riegos[i +1];
  }
  riegos[4] = tiempoActual;
<<<<<<< HEAD
  ultimaHoraRiego = hour();
}

public void guardaHistorial(){
  historial[hour()] = new Archivo_historial(valTemperatura, valHumedadAire, valHumedadTierra, valLuz, seHaRegado());
  ultimaHora = hour();
}

public boolean seHaRegado(){
  if(ultimaHoraRiego == hour()) return true;
  else return false;
}
=======
}
>>>>>>> 8b6a68ad95708575a0a69ca062cfde23d52fcf01
