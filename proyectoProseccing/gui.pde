/* =========================================================
 * ====                   WARNING                        ===
 * =========================================================
 * The code in this tab has been generated from the GUI form
 * designer and care should be taken when editing this file.
 * Only add/edit code inside the event handlers i.e. only
 * use lines between the matching comment tags. e.g.

 void myBtnEvents(GButton button) { //_CODE_:button1:12356:
     // It is safe to enter your event code here  
 } //_CODE_:button1:12356:
 
 * Do not rename this tab!
 * =========================================================
 */

public void panel1_Click1(GPanel source, GEvent event) { //_CODE_:panel1:248064:
  println("panel1 - GPanel >> GEvent." + event + " @ " + millis());
} //_CODE_:panel1:248064:

public void currentState_click(GButton source, GEvent event) { //_CODE_:currentState:804315:
  println("currentState - GButton >> GEvent." + event + " @ " + millis());
  humedadTierraRegar.setVisible(false);
  Regar_boton.setVisible(false);
  ultimoRiego.setVisible(false);
  noInt3.setVisible(false);  
  labelHumedadTierraRegar.setVisible(false);
  showData = "stats";
  
  informacionRiego.setVisible(false);
  autoRiego.setVisible(false);
} //_CODE_:currentState:804315:

public void historico_click(GButton source, GEvent event) { //_CODE_:historico:829928:
  println("button2 - GButton >> GEvent." + event + " @ " + millis());
  humedadTierraRegar.setVisible(false);
  Regar_boton.setVisible(false);
  labelHumedadTierraRegar.setVisible(false);
  ultimoRiego.setVisible(false);
  noInt3.setVisible(false);
  informacionRiego.setVisible(false);
  autoRiego.setVisible(false);

  showData = "historico";
} //_CODE_:historico:829928:

public void regar(GButton source, GEvent event) { //_CODE_:riego:708808:
  println("riego - GButton >> GEvent." + event + " @ " + millis());
  showData = "regar";
  humedadTierraRegar.setVisible(true);
  Regar_boton.setVisible(true);
  labelHumedadTierraRegar.setVisible(true);
  ultimoRiego.setVisible(true);
  noInt3.setVisible(true);
  informacionRiego.setVisible(true);
  autoRiego.setVisible(true);
} //_CODE_:riego:708808:

public void humedadTierraRegar_Metodo(GTextField source, GEvent event) { //_CODE_:humedadTierraRegar:949940:
  println("textfield3 - GTextField >> GEvent." + event + " @ " + millis());
  try {
    noInt3.setVisible(false);
    
    actualizacionValores=System.currentTimeMillis();
    valorHumedadTierraRiego = Integer.parseInt(humedadTierraRegar.getText());
  }
  catch(Exception e) {
    valorHumedadTierraRiego = 0;
    noInt3.setVisible(true);
  }
} //_CODE_:humedadTierraRegar:949940:

public void activarRiego_boton(GButton source, GEvent event) { //_CODE_:Regar_boton:478102:
  println("Regar_boton - GButton >> GEvent." + event + " @ " + millis());

  ultimoRiego.setVisible(true);
  ultimoRiego.setText("Ultimo riego realizado el " + tiempoActual);
  regarManual= true;
} //_CODE_:Regar_boton:478102:

public void autoRiego_clicked(GCheckbox source, GEvent event) { //_CODE_:autoRiego:803976:
  println("autoRiego - GCheckbox >> GEvent." + event + " @ " + millis());
  autoRiegoActivado = autoRiego.isSelected();
} //_CODE_:autoRiego:803976:



// Create all the GUI controls. 
// autogenerated do not edit
public void createGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setCursor(ARROW);
  surface.setTitle("Sketch Window");
  panel1 = new GPanel(this, 1, 0, 120, 720, "Opciones");
  panel1.setCollapsible(false);
  panel1.setDraggable(false);
  panel1.setText("Opciones");
  panel1.setTextBold();
  panel1.setOpaque(true);
  panel1.addEventHandler(this, "panel1_Click1");
  currentState = new GButton(this, 10, 34, 100, 45);
  currentState.setText("Estado actual");
  currentState.setTextBold();
  currentState.addEventHandler(this, "currentState_click");
  historico = new GButton(this, 10, 87, 100, 45);
  historico.setText("Histórico");
  historico.setTextBold();
  historico.addEventHandler(this, "historico_click");
  riego = new GButton(this, 10, 140, 100, 45);
  riego.setText("Regar");
  riego.setTextBold();
  riego.addEventHandler(this, "regar");
  panel1.addControl(currentState);
  panel1.addControl(historico);
  panel1.addControl(riego);
  humedadTierraRegar = new GTextField(this, 341, 165, 160, 30, G4P.SCROLLBARS_NONE);
  humedadTierraRegar.setOpaque(true);
  humedadTierraRegar.addEventHandler(this, "humedadTierraRegar_Metodo");
  Regar_boton = new GButton(this, 256, 296, 162, 54);
  Regar_boton.setText("REGAR");
  Regar_boton.addEventHandler(this, "activarRiego_boton");
  labelHumedadTierraRegar = new GLabel(this, 220, 174, 105, 20);
  labelHumedadTierraRegar.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  labelHumedadTierraRegar.setText("Humedad en tierra");
  labelHumedadTierraRegar.setOpaque(false);
  ultimoRiego = new GLabel(this, 185, 402, 292, 33);
  ultimoRiego.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  ultimoRiego.setOpaque(false);
  autoRiego = new GCheckbox(this, 250, 215, 180, 39);
  autoRiego.setIconAlign(GAlign.LEFT, GAlign.MIDDLE);
  autoRiego.setText("Activar el regado automático");
  autoRiego.setOpaque(false);
  autoRiego.addEventHandler(this, "autoRiego_clicked");
  informacionRiego = new GLabel(this, 242, 108, 211, 27);
  informacionRiego.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  informacionRiego.setText("Cantidad de humedad en tierra mínima antes de regar");
  informacionRiego.setOpaque(false);
  noInt3 = new GLabel(this, 501, 165, 80, 28);
  noInt3.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  noInt3.setText("Solo numeros");
  noInt3.setOpaque(false);
}

// Variable declarations 
// autogenerated do not edit
GPanel panel1; 
GButton currentState; 
GButton historico; 
GButton riego; 
GTextField humedadTierraRegar; 
GButton Regar_boton; 
GLabel labelHumedadTierraRegar; 
GLabel ultimoRiego; 
GCheckbox autoRiego; 
GLabel informacionRiego; 
GLabel noInt3; 